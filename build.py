import json
import os
import subprocess
from collections import defaultdict
from multiprocessing import Pool, cpu_count

EXPORT_PATHS = "node_modules/@keycloak/keycloak-admin-client"
OUTPUT_FILE = "dist/index.d.ts"


def main():
    prepare()
    exports = get_exports()
    dex, iex = analyze_exports(exports)
    write_statements(dex, iex)
    print(f"{OUTPUT_FILE} file has been generated successfully!")
    build()


def prepare():
    if not os.path.exists(os.path.dirname(OUTPUT_FILE)):
        os.makedirs(os.path.dirname(OUTPUT_FILE))
    if os.path.exists(OUTPUT_FILE):
        os.remove(OUTPUT_FILE)


def get_exports():
    export_files = extract_exports(EXPORT_PATHS)
    with Pool(max(cpu_count(), 1)) as p:
        exports = p.map(get_exports_from_dts, export_files)
    return exports


def extract_exports(path):
    files_containing_exports = []
    for root, _, files in os.walk(path):
        for file in files:
            abs_file = os.path.join(root, file)
            is_exported = should_file_be_exported(abs_file)
            if is_exported:
                files_containing_exports.append(abs_file)
    return files_containing_exports


def should_file_be_exported(file):
    if file.endswith(".d.ts"):
        with open(file) as f:
            return check_if_file_contains_exports(f)
    return False


def check_if_file_contains_exports(f):
    for line in f:
        if "export" in line:
            return True
    return False


def get_exports_from_dts(file_path):
    try:
        result = subprocess.run(["node", "extractExports.js", file_path], capture_output=True, text=True, check=True)
        exports = json.loads(result.stdout)
        res = dict()
        res["file"] = file_path.replace("node_modules/", "").replace(".d.ts", "")
        res["exports"] = exports
        return res
    except subprocess.CalledProcessError as e:
        print(f"Error calling Node.js script: {e}")
        return {}


def analyze_exports(exports):
    dex = defaultdict(list)
    iex = defaultdict(list)
    parsed_export_objects = set()
    for export in exports:
        parse_export(dex, export, iex, parsed_export_objects)
    return dict(dex), dict(iex)


def parse_export(dex, export, iex, parsed_export_objects):
    extracted_file_name = export["file"]
    for obj in export["exports"]:
        parse_obj(dex, extracted_file_name, iex, obj, parsed_export_objects)


def parse_obj(dex, extracted_file_name, iex, obj, parsed_export_objects):
    obj_name = obj["name"]
    obj_type = obj["type"]
    if obj_name not in parsed_export_objects:
        parsed_export_objects.add(obj_name)
        parse_obj_handler(dex, extracted_file_name, iex, obj_name, obj_type)


def parse_obj_handler(dex, extracted_file_name, iex, obj_name, obj_type):
    match obj_type:
        case "dex":
            dex[extracted_file_name].append(obj_name)
        case "iex":
            iex[extracted_file_name].append(obj_name)


def write_statements(dex, iex):
    write_statements_for_iex(iex)
    write_statements_for_dex(dex)


def write_statements_for_iex(iex):
    with (open(OUTPUT_FILE, "a") as f):
        exported_objs = set()
        for file_name, obj_names in iex.items():
            write_import_statements_for_iex(exported_objs, f, file_name, obj_names)
        export_statement = ", ".join(exported_objs)
        f.write(f'export {{{export_statement}}};\n')


def write_import_statements_for_iex(exported_objs, f, file_name, obj_names):
    for obj_name in obj_names:
        f.write(f'import {obj_name} from "{file_name}";\n')
        exported_objs.add(obj_name)


def write_statements_for_dex(dex):
    with (open(OUTPUT_FILE, "a") as f):
        for file_name, obj_names in dex.items():
            export_statement = ", ".join(obj_names)
            f.write(f'export {{{export_statement}}} from "{file_name}";\n')


def build():
    subprocess.run(
        [
            "yarn", "esbuild", "dist/index.d.ts",
            "--platform=node", "--bundle", "--format=cjs", "--outfile=dist/index.js"
        ]
    )


if __name__ == "__main__":
    main()
