const ts = require('typescript');
const fs = require('fs');

function extractExportedNames(filePath) {
    let fileContent;
    try {
        fileContent = fs.readFileSync(filePath, 'utf8');
    } catch (error) {
        console.error(`Error reading file: ${error.message}`);
        process.exit(1);
    }

    const sourceFile = ts.createSourceFile(filePath, fileContent, ts.ScriptTarget.Latest, true);
    const exportedNames = new Set();


    function collectExportedNames(node) {
        if (ts.isExportDeclaration(node)) {
            if (node.exportClause && ts.isNamedExports(node.exportClause)) {
                node.exportClause.elements.forEach(element => {
                    exportedNames.add({type: "dex", name: element.name.text});
                });
            }
        } else if (ts.isExportAssignment(node)) {
            if (ts.isIdentifier(node.expression)) {
                exportedNames.add({type: "iex", name: node.expression.text});
            }
        } else if (node.modifiers && node.modifiers.some(mod => mod.kind === ts.SyntaxKind.ExportKeyword)) {
            if (node.modifiers.some(mod => mod.kind === ts.SyntaxKind.DefaultKeyword)) {
                if (ts.isVariableStatement(node) ||
                    ts.isFunctionDeclaration(node) ||
                    ts.isClassDeclaration(node) ||
                    ts.isInterfaceDeclaration(node)
                ) {
                    if (node.name) {
                        exportedNames.add({type: "iex", name: node.name.text});
                    }
                }
            } else {
                if (ts.isVariableStatement(node) || ts.isFunctionDeclaration(node)) {
                    if (node.name) {
                        exportedNames.add({type: "dex", name: node.name.text});
                    }
                } else if (ts.isInterfaceDeclaration(node) || ts.isClassDeclaration(node)) {
                    if (node.name) {
                        exportedNames.add({type: "dex", name: node.name.text});
                    }
                }
            }
        }
    }

    ts.forEachChild(sourceFile, collectExportedNames);
    return Array.from(exportedNames);
}

const filePath = process.argv[2];
if (!filePath) {
    console.error('Please provide a .d.ts file path as an argument.');
    process.exit(1);
}

const res = extractExportedNames(filePath);
console.log(JSON.stringify(res));