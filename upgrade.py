import json
import subprocess


def update_package_version(package_file):
    package_info = subprocess.run(
        ["yarn", "npm", "info", "@keycloak/keycloak-admin-client", "--json"],
        stdout=subprocess.PIPE,
        check=True,
        text=True
    )
    data = json.loads(package_info.stdout)
    new_version = data["version"]
    with open(package_file, "r") as f:
        package_json = json.load(f)
    if "@keycloak/keycloak-admin-client" in package_json["dependencies"]:
        package_json["dependencies"]["@keycloak/keycloak-admin-client"] = f"^{new_version}"
        package_json["version"] = new_version
    else:
        print(f"Package @keycloak/keycloak-admin-client not found in dependencies.")
        return
    with open(package_file, 'w') as f:
        json.dump(package_json, f, indent=4)


def main():
    update_package_version("package.json")


if __name__ == "__main__":
    main()

