# KEYCLOAK ADMIN CLIENT CJS

This package provides the Keycloak Admin Client compiled in CommonJS format along with TypeScript definitions. It offers
utilities to extract TypeScript exports, upgrade dependencies, and build the package for usage in Node.js projects.

## Usage

To upgrade and build the package, run the following command:

```
yarn run upgrade
yarn run build
```